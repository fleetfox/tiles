use opengl_graphics::{
    Gl,
    Texture,
};
use graphics::*;
use piston::{
    AssetStore,
};


pub static TILE_DIMENSION: u32 = 32;

pub enum TileType {
    Blank,
    Grass,
    DirtInGrass,
}

pub struct Tile {
    tile_type: TileType,
}

impl Tile {
    pub fn new(t: TileType) -> Tile {
        Tile { 
            tile_type: t,
        }
    }
}

pub struct Map {
    tiles: [[Tile, ..32], ..32],
    tilemap: Texture,
}

impl Map {
    pub fn new(asset_store: &mut AssetStore) -> Map {
        let image = asset_store.path("tileset.png").unwrap();
        Map {
            tiles: [[Tile::new(Blank), ..32], ..32],
            tilemap: Texture::from_path(&image).unwrap(),
        }
    }

    pub fn add_tile(&mut self, x: uint, y: uint, tile: Tile) {
        self.tiles[x][y] = tile;
    }

    pub fn get_tile(&self, x: uint, y: uint) -> Option<Tile> {
        if 0 < x && x < 32
            && 0 < y && y < 32 {
            Some(self.tiles[x][y])
        } else {
            None
        }        
    }

    pub fn draw(&self, tile: Tile, x: f64, y: f64, c: &Context, gl: &mut Gl) {
        match tile.tile_type {
            Blank => { 
                c.rect(x, y, TILE_DIMENSION as f64, TILE_DIMENSION as f64).rgb(0.3, 0.3, 0.3).draw(gl);
            }
            Grass => {
                c.image(&self.tilemap)
                    .src_rect(160, 0, 32, 32)
                    .rect(
                        x.round(),
                        y.round(),
                        TILE_DIMENSION as f64,
                        TILE_DIMENSION as f64)
                    .draw(gl);
            }
            DirtInGrass => {
                c.image(&self.tilemap)
                    .src_rect(128, 0, 32, 32)
                    .rect(
                        x.round(),
                        y.round(),
                        TILE_DIMENSION as f64,
                        TILE_DIMENSION as f64)
                    .draw(gl);
            }
        }
    }
}