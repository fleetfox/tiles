use settings;
use math::Vec2f;

pub struct Camera {
    pub position: Vec2f,

    pub target: Vec2f,
    speed: f64,
}

impl Camera {
    pub fn new(x: f64, y: f64) -> Camera {
        Camera {
            position: Vec2f::new(x, y),
            target: Vec2f::new(x,y),
            speed: 0.5
        }
    }

    /*
    pub fn move(&mut self, v: Vec2f) {
        self.target = v;
        self.position = v;
    }

    pub fn move_center(&mut self, v: Vec2f) {
        let offset = Vec2f::new(
            settings::VIEWPORT_WIDTH as f64 / 2f64,
            settings::VIEWPORT_HEIGHT as f64 / 2f64
        );

        let new_pos = v - offset;

        self.target = new_pos;
        self.position = new_pos;
    }

    pub fn go_to(&mut self, v: Vec2f) {
        self.target = v;
    }
    */


    pub fn go_to_center(&mut self, v: Vec2f) {
        let offset = Vec2f::new(
            settings::VIEWPORT_WIDTH as f64 / 2f64,
            settings::VIEWPORT_HEIGHT as f64 / 2f64
        );

        self.target = v - offset + self.position;
    }

    pub fn get_bounds(&mut self) -> [int, ..4] {
        let x = (self.position.x / settings::TILE_SIZE as f64) as int;
        let y = (self.position.y / settings::TILE_SIZE as f64) as int;

        let mut w = (settings::VIEWPORT_WIDTH / settings::TILE_SIZE) as int + 2;
        let mut h = (settings::VIEWPORT_HEIGHT / settings::TILE_SIZE) as int + 2;

        if x % settings::TILE_SIZE as int != 0 {
            w = w + 1;
        }

        if y % settings::TILE_SIZE as int != 0 {
            h = h + 1;
        }
        return [x, y, w, h]
    }

    pub fn tile_offset(self) -> Vec2f {
        Vec2f::new(
            self.position.x as f64 % settings::TILE_SIZE as f64,
            self.position.y as f64 % settings::TILE_SIZE as f64,
        )
    }

    pub fn update_state(&mut self) {

        // Velocity magnitude
        let mut v: f64;

        let path = self.target - self.position;
        let distance = path.length();

        //If we're within 1 pixel of the target already, just snap
        //to target and stay there. Otherwise, continue
        if distance <= 1.0 {
            self.position = self.target
        } else {

            //We set our velocity to move 1/60th of the distance to
            //the target. 60 is arbitrary, I picked it because I intend
            //to run this function once every 60th of a second. We also
            //allow the user to change the camera speed via the speed member
            v = distance/10.00 * self.speed;

            //Keep v above 1 pixel per update, otherwise it may never get to
            //the target. v is an absolute value thanks to the squaring of x
            //and y earlier
            if v < 1.0 {
                v = 1.0;
            }
            
            //Similar triangles to get vx and vy
            let travel = Vec2f::new(path.x * (v/distance), path.y * (v/distance));

            //Then update camera's position and we're done
            self.position = self.position + travel;
        }
    }
}