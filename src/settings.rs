pub static VIEWPORT_WIDTH: u32 = 800;
pub static VIEWPORT_HEIGHT: u32 = 600;
pub static TILE_SIZE: u32 = 32;