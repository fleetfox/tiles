use camera::Camera;
use map::{
    Map,
    Tile,
    Grass,
    DirtInGrass,
};

use math::Vec2f;

use graphics::*;

use opengl_graphics::Gl;

use piston::{
    AssetStore,
    MouseMoveArgs,
};

use piston::keyboard;


use settings;

pub struct Engine {
    map: Option<Map>,
    camera: Option<Camera>,
    asset_store: Option<AssetStore>,
    mouse_position: Vec2f,
}

impl Engine {
    pub fn new() -> Engine {
        Engine {
            map: None,
            camera: None,
            asset_store: None,
            mouse_position: Vec2f::new(0.0, 0.0),
        }
    }

    pub fn load(&mut self) {
        self.asset_store = Some(AssetStore::from_folder("../assets"));
        self.map = Some(Map::new(self.asset_store.get_mut_ref()));
        self.camera = Some(Camera::new(0.0, 0.0))
    }

    pub fn go_to_mouse(&mut self) {

        self.camera
            .get_mut_ref()
            .go_to_center(self.mouse_position);
    }

    pub fn move_mouse(&mut self, mouse_args: &MouseMoveArgs) {
        self.mouse_position = Vec2f::new(mouse_args.x, mouse_args.y);
    }

    pub fn update(&mut self) {
        self.camera.get_mut_ref().update_state();
    }

    pub fn key_press(&mut self, k: keyboard::Key) {
        println!("Press: {:?}", k);
    }

    pub fn key_release(&mut self, k: keyboard::Key) {
        match k {
            keyboard::Space => {
                let camera = self.camera.get_mut_ref();
                println!(
                    "Bounds: {:?}",
                    camera.get_bounds()
                );
                println!(
                    "Camera target at {:?}",
                    camera.target
                );
            }
            _ => {
                println!("Release: {:?}", k);
            }
        }
    }

    pub fn render(&mut self, c: &Context, gl: &mut Gl) {

        let grass = Tile::new(Grass);
        let dirt_spot = Tile::new(DirtInGrass);

        let map = self.map.get_mut_ref();

        //map.add_tile(0, 0, t);
        map.add_tile(1, 1, grass);
        map.add_tile(1, 2, grass);
        map.add_tile(1, 3, grass);
        map.add_tile(1, 4, grass);
        map.add_tile(1, 5, grass);
        map.add_tile(1, 6, grass);

        map.add_tile(2, 1, grass);
        map.add_tile(2, 2, dirt_spot);
        map.add_tile(2, 3, grass);
        map.add_tile(2, 4, grass);
        map.add_tile(2, 5, dirt_spot);
        map.add_tile(2, 6, grass);

        map.add_tile(3, 1, grass);
        map.add_tile(3, 2, grass);
        map.add_tile(3, 3, grass);
        map.add_tile(3, 4, grass);
        map.add_tile(3, 5, grass);
        map.add_tile(3, 6, grass);


        map.add_tile(16, 16, grass);

        let camera = self.camera.get_mut_ref();

        // [x, y, w, h]
        let bounds = camera.get_bounds();

        let offset = camera.tile_offset();
        //println!("{:?}", bounds);
        let mut y = 0; 
        let mut tile_y = bounds[1];

        while y < bounds[3] {
            //println!("{:?}", y);
            let mut x = 0;
            let mut tile_x = bounds[0];

            while x < bounds[2] {
                //println!("{:?}", [x, y]);
                // Make sure we only render tiles in map
                // maybe move this in get tile Some(tile)
                match map.get_tile(tile_x as uint, tile_y as uint) {
                    Some(tile) => {
                        map.draw(
                            tile,
                            (x as u32 * settings::TILE_SIZE) as f64 - offset.x,
                            (y as u32 * settings::TILE_SIZE) as f64 - offset.y,
                            c,
                            gl
                        );
                    }
                    None => {}
                }

                x += 1;
                tile_x += 1;
            }

            y += 1;
            tile_y += 1;
        };
    }
}