#![feature(globs)]

extern crate graphics;
extern crate piston;
extern crate debug;
extern crate sdl2_game_window;
extern crate opengl_graphics;

use piston::{
    GameIterator,
    GameIteratorSettings,
    GameWindowSettings,
    Render,
    Update,
    KeyPress,
    KeyRelease,
    MouseRelease,
    MouseMove,
};

use sdl2_game_window::GameWindowSDL2;
use opengl_graphics::Gl;
use graphics::*;

mod settings;
mod map;
mod math;
mod camera;
mod engine;

#[allow(unused_variable)]
fn main() {
    use engine::Engine;
    let mut game_window: GameWindowSDL2 = GameWindowSDL2::new(
        GameWindowSettings {
            title: "Image".to_string(),
            size: [settings::VIEWPORT_WIDTH, settings::VIEWPORT_HEIGHT],
            fullscreen: false,
            exit_on_esc: true,
        }
    );

    let mut app = Engine::new();
    app.load();

    let mut game_iterator = GameIterator::new(&mut game_window,
        &GameIteratorSettings {
            updates_per_second: 120,
            max_frames_per_second: 30
        });

    let ref mut gl = Gl::new();

    loop { 
        match game_iterator.next() { 
            None => {
                break
            },
            Some(e) => {
                match e {
                    Render(args) => {
                        let c = graphics::Context::abs(args.width as f64, args.height as f64);
                        c.rgb(0.25, 0.25, 0.25).draw(gl);
                        app.render(&c, gl); 
                    },

                    Update(args) => {
                        app.update();
                    },
                    KeyPress(args) => {
                        app.key_press(args.key);
                    },
                    KeyRelease(args) => {
                        app.key_release(args.key);
                    },
                    MouseRelease(args) => {
                        app.go_to_mouse();
                    }
                    MouseMove(args) => {

                        app.move_mouse(&args);
                    }
                    _ => {},
                }
            }
        }
    }
}