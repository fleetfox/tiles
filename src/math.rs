pub struct Vec2f {
    pub x: f64,
    pub y: f64,
}

impl Vec2f {
    pub fn new(x: f64, y: f64) -> Vec2f {
        Vec2f { 
            x: x,
            y: y,
        }
    }

    pub fn length(&self) -> f64 {
        ((self.x * self.x) + 
            (self.y * self.y)).sqrt()
    }
}

impl Add<Vec2f, Vec2f> for Vec2f {
    fn add(&self, other: &Vec2f) -> Vec2f {
        Vec2f {
            x: self.x + other.x,
            y: self.y + other.y
        }
    }
}

impl Sub<Vec2f, Vec2f> for Vec2f {
    fn sub(&self, other: &Vec2f) -> Vec2f {
        Vec2f {
            x: self.x - other.x,
            y: self.y - other.y
        }
    }
}